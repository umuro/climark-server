from django.http import JsonResponse

from rest_framework.generics import GenericAPIView
import requests

class AWhereTokenView(GenericAPIView):
    def get(self, request, **kwargs):
        url = "https://api.awhere.com/oauth/token"
        payload = "grant_type=client_credentials"
        headers = {
            'Content-Type': "application/x-www-form-urlencoded",
            'Authorization': "Basic bDhla3p0U3BNa25oZ3lqTG55ZVpBV1VWdHcyajFRclo6NGhwT3dxMGI5SDVEZDZobw==",
            'Cache-Control': "no-cache",
        }

        response = requests.request("POST", url, data=payload, headers=headers)
        
        return JsonResponse(response.json())
