from django.conf.urls import url, include

from auth.api import AWhereTokenView

app_name = 'auth'


api_urls = [
    url(r'awheretoken/', AWhereTokenView.as_view(), name="get-awhere-token"),
]

