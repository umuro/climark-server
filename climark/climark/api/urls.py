from django.conf.urls import url, include

from auth.urls import api_urls as auth_api_urls


urlpatterns = [
    url(r'auth/', include(auth_api_urls)),
]
